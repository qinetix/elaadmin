$(function() {

	if ($('table').length > 0) {
		$.each($('table:not(.norender)'), function() {
			var sort = $(this).data('sort');
			if (sort == undefined) {
				sort = 'desc';
			}
			$(this).DataTable({
				"order": [
					[0, sort]
				],
				"iDisplayLength": 50,
				"stateSave": true
			});
		});
	}
});
