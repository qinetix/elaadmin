$(function() {
	if ($(".js-typeahead-company").length > 0) {
		$(".js-typeahead-company")
			.on('focus', this, function(){
				$(this).val('');
			});
		$.typeahead({
			input: ".js-typeahead-company",
			minLength: 1,
			maxItem: 30,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			filter: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				company: {
					display: [ "Company.ComName", "Company._id" ],
					ajax: {
						url: "/company/api",
						type: 'POST',
						data: {
							api: 'AutoComplete',
							token: '{{query}}'
						},
						path: 'Companies'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					window.location.href = "/company/edit/" + item.Company._id;
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	$('body')
		.on('click', '.delete-company', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要刪除[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Delete");
				data.append("id", id);
				$.ajax({
					url: '/company/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('click', '.verify-company', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要驗證[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Verify");
				data.append("id", id);
				data.append("verified", true);
				$.ajax({
					url: '/company/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('change', '.toggle-verify-company', function() {
			var id = $(this).data('id');
			var verified = $(this).prop("checked");
			var data = new FormData();
			data.append("api", "Verify");
			data.append("id", id);
			data.append("verified", verified);
			$.ajax({
				url: '/company/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})
		.on('change', '.toggle-disabled-company', function() {
			var id = $(this).data('id');
			var disabled = !$(this).prop("checked");
			var data = new FormData();
			data.append("api", "Disable");
			data.append("id", id);
			data.append("disabled", disabled);
			$.ajax({
				url: '/company/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})
		.on('click', '.update-company', function() {
			var id = $(this).data('id');
			var data = new FormData();
			data.append("api", "Update");
			data.append("id", id);
			data.append("Name", $('#Name').val());
			data.append("Capital", $('#Capital').val());
			data.append("Industry", $('#Industry').val());
			data.append("Address", $('#Address').val());
			data.append("Headcount", $('#Headcount').val());
			data.append("Contact", $('#Contact').val());
			data.append("Phone", $('#Phone').val());
			data.append("Fax", $('#Fax').val());
			data.append("Site", $('#Site').val());
			data.append("Intro", $('#Intro').val());
			data.append("IndustryDesc", $('#IndustryDesc').val());
			data.append("Business", $('#Business').val());
			data.append("Welfare", $('#Welfare').val());
			$.ajax({
				url: '/company/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					if (data.Status) {
						alert("更新成功");
						window.location.reload();
					} else {
						alert("更新失敗");
					}
				}
			});
		});
});
