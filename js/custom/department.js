$(function() {

	if ($(".js-typeahead-department").length > 0) {
		var schoolID = $(".js-typeahead-department").data('schoolID');
		$.typeahead({
			input: ".js-typeahead-department",
			minLength: 1,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				department: {
					display: "Name",
					ajax: {
						url: "/department/api",
						type: 'POST',
						data: {
							api: 'AutoComplete',
							token: '{{query}}',
							schoolID: schoolID
						},
						path: 'Departments'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					window.location.href = "/department/edit/" + item._id;
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	$('body')
		.on('click', '.delete-department', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要刪除[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Delete");
				data.append("id", id);
				$.ajax({
					url: '/department/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('click', '.verify-department', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要驗證[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Verify");
				data.append("id", id);
				data.append("verified", true);
				$.ajax({
					url: '/department/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('change', '.toggle-verify-department', function() {
			var id = $(this).data('id');
			var verified = $(this).prop("checked");
			var data = new FormData();
			data.append("api", "Verify");
			data.append("id", id);
			data.append("verified", verified);
			$.ajax({
				url: '/department/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})
		.on('click', '.update-department', function() {
			var id = $(this).data('id');
			var data = new FormData();
			data.append("api", "Update");
			data.append("id", id);
			data.append("Name", $('#Name').val());
			data.append("Code", $('#Code').val());
			data.append("Site", $('#Site').val());
			data.append("Email", $('#Email').val());
			data.append("Tel", $('#Tel').val());
			data.append("TelExt", $('#TelExt').val());
			data.append("Fax", $('#Fax').val());

			var cates = $.unique($.map($('input[name="Cate"]'), function(cate){ return $.trim($(cate).val()).length > 0 ? $.trim($(cate).val()) : null; }));
			for(var i=0; i<cates.length; i++){
				data.append("Cates[]", cates[i]);
			}

			$.ajax({
				url: '/department/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					if (data.Status) {
						alert("更新成功");
						window.location.reload();
					} else {
						alert("更新失敗");
					}
				}
			});
		})
		.on('click', '.toggle-add-department-cate', function(){
			$(this).before(
				$('<input name="Cate" type="text" placeholder="" class="form-control">')
			);
		});
});
