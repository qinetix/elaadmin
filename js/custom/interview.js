$.extend({
	RatingStars: {
		bind: function(selector) {
			var This = $(selector);
			This
				.on('mouseover', 'i', function() {
					$.RatingStars.renderHover($(this));
				})
				.on('mouseout', this, function() {
					$.RatingStars.renderChecked(This);
				})
				.on('click', 'i', function() {
					$(this).parent().find('i.checked').removeClass('checked');
					$(this).addClass('checked');
					var index = This.find('i').index($(this));
					var rating = index + 1;
					var r = confirm("確定要設為精選(" + rating + ")顆星？");
					if (r == true) {
						var id = $(this).parent().data('id');
						var data = new FormData();
						data.append("api", "FeatureRate");
						data.append("id", id);
						data.append("featured", true);
						data.append("rating", rating);
						$.ajax({
							url: '/interview/api',
							data: data,
							processData: false,
							contentType: false,
							type: 'POST',
							success: function(data) {
								if (data.Status) {
									alert("更新成功");
								} else {
									alert("更新失敗");
								}
								window.location.reload();
							}
						});
					}
				});
			$.RatingStars.renderChecked(This);
		},
		renderChecked: function($container) {
			var $checked = $container.find('i.checked');
			var index = $container.find('i').index($checked);
			for (var i = 0; i <= 4; i++) {
				if (i <= index) {
					$container.find('i:eq(' + i + ')').css('color', '#daa520');
				} else {
					$container.find('i:eq(' + i + ')').css('color', '#67757c');
				}
			}
		},
		renderHover: function($star) {
			var $container = $star.parent();
			var index = $container.find('i').index($star);
			for (var i = 0; i <= 4; i++) {
				if (i <= index) {
					$container.find('i:eq(' + i + ')').css('color', '#daa520');
				} else {
					$container.find('i:eq(' + i + ')').css('color', '#67757c');
				}
			}
		}
	}

});

function incrUserBadge(uid) {
	var badge = prompt("設為精選要獎勵 " + uid + " 多少積分", 0);
	var data = new FormData();
	data.append("api", "IncrBadge");
	data.append("id", uid);
	data.append("badge", badge);
	$.ajax({
		url: '/user/api',
		data: data,
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data) {
			// window.location.reload();
			alert("已獎勵 " + badge.toString() + " 積分");
		}
	});
}

$(function() {

	if ($(".js-typeahead-company").length > 0) {
		// $(".js-typeahead-company")
		// 	.on('focus', this, function(){
		// 		$(this).val('');
		// 	});
		$.typeahead({
			input: ".js-typeahead-company",
			minLength: 1,
			maxItem: 30,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			filter: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				company: {
					display: ["Company.ComName", "Company._id"],
					ajax: {
						url: "/company/api",
						type: 'POST',
						data: {
							api: 'AutoComplete',
							token: '{{query}}'
						},
						path: 'Companies'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					$('#Cid').val(item.Company._id);
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	$('body')
		.on('click', '.delete-interview', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要刪除[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Delete");
				data.append("id", id);
				$.ajax({
					url: '/interview/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('click', '.verify-interview', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要驗證[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Verify");
				data.append("id", id);
				data.append("verified", true);
				$.ajax({
					url: '/interview/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('change', '.toggle-verify-interview', function() {
			var id = $(this).data('id');
			var verified = $(this).prop("checked");
			var data = new FormData();
			data.append("api", "Verify");
			data.append("id", id);
			data.append("verified", verified);
			$.ajax({
				url: '/interview/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})
		.on('change', '.toggle-feature-interview', function() {
			var id = $(this).data('id');
			var uid = $(this).data('uid');
			var featured = $(this).prop("checked");
			var data = new FormData();
			data.append("api", "Feature");
			data.append("id", id);
			data.append("featured", featured);
			$.ajax({
				url: '/interview/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
					if (featured) {
						incrUserBadge(uid);
					}
				}
			});
		})
		.on('change', '.toggle-hide-interview', function() {
			var id = $(this).data('id');
			var hide = !$(this).prop("checked");
			var data = new FormData();
			data.append("api", "Hide");
			data.append("id", id);
			data.append("hide", hide);
			$.ajax({
				url: '/interview/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})

		.on('click', '.update-interview', function() {
			var id = $(this).data('id');
			var data = new FormData();
			data.append("api", "Update");
			data.append("id", id);
			data.append("cid", $('#Cid').val());
			$.ajax({
				url: '/interview/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					if (data.Status) {
						alert("更新成功");
						window.location.reload();
					} else {
						alert("更新失敗");
					}
				}
			});
		})
		.on('click', 'label.featured', function() {
			var id = $(this).data('id');
			var r = confirm("確定要取消精選？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Feature");
				data.append("id", id);
				data.append("featured", false);
				$.ajax({
					url: '/interview/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						if (data.Status) {
							alert("更新成功");
							window.location.reload();
						} else {
							alert("更新失敗");
						}
					}
				});
			}
		});

	$.RatingStars.bind('.feature-ratings');

	$('#ywnSelector').val($('#ywnSelector').data("selected"));
	$('#ywnSelector').change(function(){
		let ywn = $(this).val();
		window.location.href = "/interview/weeklyReport/" + ywn.split("/")[0] + "/" + ywn.split("/")[1]

	});
});
