function setSchool(uid, schoolID) {
	var data = new FormData();
	data.append("api", "SetSchool");
	data.append("id", uid);
	data.append("schoolID", schoolID);
	$.ajax({
		url: '/user/api',
		data: data,
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data) {
			window.location.reload();
		}
	});
}

function setDepartment(uid, departmentID) {
	var data = new FormData();
	data.append("api", "SetDepartment");
	data.append("id", uid);
	data.append("departmentID", departmentID);
	$.ajax({
		url: '/user/api',
		data: data,
		processData: false,
		contentType: false,
		type: 'POST',
		success: function(data) {
			window.location.reload();
		}
	});
}

$(function() {

	if ($(".js-typeahead-school").length > 0) {
		$.typeahead({
			input: ".js-typeahead-school",
			minLength: 1,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				school: {
					display: "Name",
					ajax: {
						url: "/school/api",
						type: 'POST',
						data: {
							api: 'AutoComplete',
							token: '{{query}}'
						},
						path: 'Schools'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					var uid = $(node).data('id');
					var schoolID = item._id;
					setSchool(uid, schoolID);
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	if ($(".js-typeahead-department").length > 0) {
		var schoolID = $(".js-typeahead-department").data('school-id');
		$.typeahead({
			input: ".js-typeahead-department",
			minLength: 1,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				department: {
					display: "Name",
					ajax: {
						url: "/department/api?1",
						type: 'POST',
						data: {
							api: 'AutoCompleteBySchoolID',
							token: '{{query}}',
							schoolID: schoolID
						},
						path: 'Departments'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					var uid = $(node).data('id');
					var departmentID = item._id;
					setDepartment(uid, departmentID);
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	$('body')
		.on('click', '.toggle-lock-user', function() {
			var id = $(this).data('id');
			var locked = !$(this).data("locked");
			var data = new FormData();
			data.append("api", "Lock");
			data.append("id", id);
			data.append("locked", locked);

			var This = $(this);

			$.ajax({
				url: '/user/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
					if (locked) {
						This.removeClass("btn-success").addClass("btn-danger");
						This.find("span").text("已");
					} else {
						This.removeClass("btn-danger").addClass("btn-success");
						This.find("span").text("未");
					}
					This.data("locked", locked);
				}
			});
		})
		.on('click', '.toggle-delete-user', function() {
			var id = $(this).data('id');
			var r = confirm("確定要刪除帳號[" + id + "]");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Delete");
				data.append("id", id);

				var This = $(this);

				$.ajax({
					url: '/user/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('click', '.memo-user', function() {
			var id = $(this).data('id');
			var memo = $.trim($(this).find('.userMemo').text());
			var newMemo = prompt("請為 " + id + " 輸入 Memo", memo);

			var This = $(this);

			if (newMemo != null) {
				var data = new FormData();
				data.append("api", "Memo");
				data.append("id", id);
				data.append("memo", newMemo);
				$.ajax({
					url: '/user/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						// window.location.reload();
						This.find('.userMemo').text(newMemo);
					}
				});

			}
		})
		.on('click', '.set-badge-user', function() {
			var id = $(this).data('id');
			var badge = $.trim($(this).find('.userBadge').text());
			var newBadge = prompt("請為 " + id + " 輸入積分", badge);

			var This = $(this);

			if (newBadge != null) {
				var data = new FormData();
				data.append("api", "SetBadge");
				data.append("id", id);
				data.append("badge", newBadge);
				$.ajax({
					url: '/user/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						// window.location.reload();
						This.find('.userBadge').text(newBadge);
					}
				});
			}
		})
		.on('click', '.edit-user-card', function() {
			$(this).parent().find('.typeahead__container').toggleClass('hide');
			if (!$(this).parent().find('.typeahead__container').hasClass('hide')) {
				$(this).parent().find('.typeahead__container input').focus().select();
			}
		});
});
