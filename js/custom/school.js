$(function() {
	if ($(".js-typeahead-school").length > 0) {
		$.typeahead({
			input: ".js-typeahead-school",
			minLength: 1,
			order: "asc",
			dynamic: true,
			cancelButton: false,
			delay: 500,
			backdrop: {
				"background-color": "#fff"
			},
			source: {
				school: {
					display: "Name",
					ajax: {
						url: "/school/api",
						type: 'POST',
						data: {
							api: 'AutoComplete',
							token: '{{query}}'
						},
						path: 'Schools'
					}
				}
			},
			callback: {
				onClick: function(node, a, item, event) {
					window.location.href = "/school/edit/" + item._id;
				},
				onCancel: function(node, a, item, event) {}
			}
		});
	}

	$('body')
		.on('click', '.delete-school', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要刪除[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Delete");
				data.append("id", id);
				$.ajax({
					url: '/school/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('click', '.verify-school', function() {
			var id = $(this).data('id');
			var name = $(this).data('name');
			var r = confirm("確定要驗證[" + name + "]？");
			if (r == true) {
				var data = new FormData();
				data.append("api", "Verify");
				data.append("id", id);
				data.append("verified", true);
				$.ajax({
					url: '/school/api',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					success: function(data) {
						window.location.reload();
					}
				});
			}
		})
		.on('change', '.toggle-verify-school', function() {
			var id = $(this).data('id');
			var verified = $(this).prop("checked");
			var data = new FormData();
			data.append("api", "Verify");
			data.append("id", id);
			data.append("verified", verified);
			$.ajax({
				url: '/school/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					// window.location.reload();
				}
			});
		})
		.on('click', '.update-school', function() {
			var id = $(this).data('id');
			var data = new FormData();
			data.append("api", "Update");
			data.append("id", id);
			data.append("Name", $('#Name').val());
			data.append("Code", $('#Code').val());
			data.append("Description", $('#Description').val());
			data.append("Country", $('#Country').val());
			data.append("System", $('#System').val());
			data.append("Setup", $('#Setup').val());
			$.ajax({
				url: '/school/api',
				data: data,
				processData: false,
				contentType: false,
				type: 'POST',
				success: function(data) {
					if (data.Status) {
						alert("更新成功");
						window.location.reload();
					} else {
						alert("更新失敗");
					}
				}
			});
		});
});
