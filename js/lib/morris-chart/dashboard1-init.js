// Dashboard 1 Morris-chart
$( function () {
	"use strict";

	// Extra chart
	Morris.Area( {
		element: 'extra-area-chart',
		data: chart_data,
		lineColors: [ '#26DAD2' ],
		xkey: 'Date',
		ykeys: [ 'Count' ],
		labels: [ 'Count' ],
		pointSize: 0,
		lineWidth: 0,
		resize: true,
		fillOpacity: 0.8,
		behaveLikeLine: true,
		gridLineColor: '#e0e0e0',
		hideHover: 'auto'

	} );
} );
